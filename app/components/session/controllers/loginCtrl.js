var loginCtrl 	= function  () {
	console.log("login Ctrl ::");

	var loginCont 	= $('#login--container');
	var VIEWS 		= APP + "session/views/";

	console.log("where is views? : ", VIEWS);

	var formCont = $.get(VIEWS + "form.html");

	formCont.then( function (form){
		// console.log("formCont: ", form);
		var formResult = $(form);
		
		// STYLING
		formResult.find('input').addClass('form-control');
		formResult.find('button').addClass('btn btn-success');

		loginCont.html(formResult);

		formResult.submit( function (){
			console.log("form submit ::");
			var frm  	= $(this);
			var usr 	= frm.find('#usuario').val();
			var psw 	= frm.find('#clave').val();

			var userPost = {
				"user": usr,
				"password": psw
			};

			console.log("userPost: ", userPost);

			var ajaxReq = $.ajax({
				url: API + "users.json",
				type: 'post',
				data: JSON.stringify(userPost),
				dataType: "json",
				contentType: "application/json"
			});
			
			ajaxReq.done( function (data){
				console.log("result ajax ::", data);
				if(data.user == userPost.user && data.password == userPost.password){
					// alert("Ingreso correcto.");
					loginCont.hide();
					dashboardCont.show();
				} else {
					alert("Error: usuario y/o clave incorrecta.");
				}
			});

			return false;
		})

		console.log("form selector: ", $(form), " formResult: ", formResult);
		
	})

};