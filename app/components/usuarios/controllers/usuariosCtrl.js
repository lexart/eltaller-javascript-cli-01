var usuariosCtrl = function  () {
	console.log("usuarios ::");

	// 
	var usuariosCont = $('#usuarios--container');
	var VIEWS 		 = APP + "usuarios/views/";
	var listadoCont  = $.get(VIEWS + "indexView.html");
	var usuariosMl 	 = $.get(API  + "usuarios.json");

	listadoCont.then( function (html){
		var listado 	= $(html);
		usuariosCont.html(listado);
		var tbodyCont 	= usuariosCont.find("table tbody");
		var tr 			= "";
		var formUsuario = listado.find('#form--usuario');

		$('#usr--add').click( function (){
			formUsuario.find("input[name=id]").val("");
		})

		console.log("run tabla ::", abmCont, tbodyCont);

		usuariosMl.then( function (res){
			console.log("usuarios: ", res);

			for (var i = res.length - 1; i >= 0; i--) {
				tr += "<tr id='trUser-"+res[i].id+"'>";
				tr += "<td>" + res[i].id 		+ "</td>";
				tr += "<td>" + res[i].nombre 	+ "</td>";
				tr += "<td>" + res[i].apellido 	+ "</td>";
				tr += "<td>" + res[i].email 	+ "</td>";
				tr += "<td>" + res[i].telefono 	+ "</td>";
				tr += "<td>" + "<button class='btn btn-primary editar_usr' data-toggle='modal' data-target='#exampleModal' id='user-"+res[i].id+"'>Editar</button>" + "</td>";
				tr += "</tr>";
			};

			console.log("result TR: ", tr);
			tbodyCont.html( tr );
			$(abmCont).show();

			var btnEditar 	= listado.find('.editar_usr');

			btnEditar.click( function (){
				console.log("edit user ::");
				var ths = $(this);
				var id = ths.attr('id').split('-')[1];
				var user;

				res.map( function (item){
					if(id == item.id){
						user = item;
						return;
					}
				});

				formUsuario.find('input[name=id]').val(user.id);
				formUsuario.find('input[name=nombre]').val(user.nombre);
				formUsuario.find('input[name=apellido]').val(user.apellido);
				formUsuario.find('input[name=email]').val(user.email);
				formUsuario.find('input[name=telefono]').val(user.telefono);

				console.log("user: ", user);
			});
		})

		

		formUsuario.submit( function (){
			var ths 	= $(this);

			var id 		= ths.find('input[name=id]').val();
			var nombre 	= ths.find('input[name=nombre]').val();
			var apellido= ths.find('input[name=apellido]').val();
			var email 	= ths.find('input[name=email]').val();
			var telefono= ths.find('input[name=telefono]').val();

			var usrPost = {
					"id": id,
					"nombre": 	nombre,
					"apellido": apellido,
					"email": 	email,
					"telefono": telefono
				};

			if(!id){
				tr = "";

				tr += "<tr>";
				tr += "<td>" + "123" + "</td>";
				tr += "<td>" + usrPost.nombre 	+ "</td>";
				tr += "<td>" + usrPost.apellido 	+ "</td>";
				tr += "<td>" + usrPost.email 	+ "</td>";
				tr += "<td>" + usrPost.telefono 	+ "</td>";
				tr += "<td>" + "<button class='btn btn-primary'>Editar</button>" + "</td>";
				tr += "</tr>";

				tbodyCont.append( tr );

				console.log("nombre: ", usrPost);
			
			} else {
				var idRow = id;
				var row   = $("#trUser-"+ idRow);
				var tds   = row.find('td');

				tds.eq(1).text(usrPost.nombre);
				tds.eq(2).text(usrPost.apellido);
				tds.eq(3).text(usrPost.email);
				tds.eq(4).text(usrPost.telefono);

				// console.log( tds.eq(1).text() );
			}
			

			$('.btn.btn-secondary').click()
			return false;
		});

		
		
	});
};