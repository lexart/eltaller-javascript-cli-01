var app = angular.module('app',['ui.router', 'naif.base64']);
var API = "http://localhost/eltaller-php-backend/";

app.config( function($stateProvider, $urlRouterProvider) {
  var login = {
    name: 'login',
    url: '/',
    templateUrl: 'app/components/session/views/indexView.html',
    controller: 'LoginCtrl'
  }

  var usuarios = {
    name: 'usuarios',
    url: '/usuarios/all',
    templateUrl: 'app/components/usuarios/views/indexView.html',
    controller: 'UsuariosCtrl'
  }

  var usuarioEdit = {
    name: 'usuarioEdit',
    url: '/usuario/edit/:id',
    templateUrl: 'app/components/usuarios/views/formView.html',
    controller: 'UsuariosCtrl'
  }

  var usuarioNew = {
    name: 'usuarioNew',
    url: '/usuario/new',
    templateUrl: 'app/components/usuarios/views/formView.html',
    controller: 'UsuariosCtrl'
  }


  $stateProvider.state(login);
  $stateProvider.state(usuarios);
  $stateProvider.state(usuarioEdit);
  $stateProvider.state(usuarioNew);
  $urlRouterProvider.otherwise('/');

});

app.controller('MainCtrl', ['$scope','$rootScope', function ($scope, $rootScope){
	console.log("MainCtrl :: ");


	$scope.titulo 		= "Main Controller";
	$scope.usuario;

	$rootScope.state 	= "login";

	if(window.localStorage["user"] == "alex"){
		$rootScope.state 	= "dashboard"
	}

	$scope.enviarDatos 	= function (){
		var coso 		= $scope.usuario;
		console.log(coso);
	};

	$scope.changeState = function (state){
		console.log("state log: ", state);
		$rootScope.state = state;
	}
}]);

console.log(app);
