app.factory('UsuariosService', function ($http){
	var service = {
		getAllUsers: function (model, cb){
			var ml = "?model="+model+"&action=single&id=1";
			$http.get(API + ml).then( function (res){
				var rx = res.data;
				cb(rx);
			});
		},
		getUserById: function (model, id, cb){
			var ml = "?model="+model+"&action=single&id="+id;
			$http.get(API + ml).then( function (res){
				var rx = res.data;
				cb(rx);
			});
		}
	};
	return service;
});

app.controller('UsuariosCtrl', ['$scope','$rootScope','$state','$stateParams','$http','UsuariosService', function($scope, $rootScope, $state, $stateParams, $http, UsuariosService){
	console.log("Usuarios ::");

	$scope.user 	= {};
	$scope.mostrarForm = false;
	$scope.error 	= "";

	var model 		= "?model=usuarios&action=single&id=1";

	$scope.toggleMostrarForm = function (){
		$scope.mostrarForm = !$scope.mostrarForm;
	};

	$scope.usuarios = [];

	UsuariosService.getAllUsers("usuarios", function (res){
		$scope.usuarios = res;
		console.log("res :", res);
	});

	$scope.editar = function (usuario){
		$scope.user 	   = usuario;
	}


	if($stateParams.id){
		$scope.error 	 = "";
		var id 		= angular.copy($stateParams.id);
		model 		= "?model=&action=single&id=";

		UsuariosService.getUserById("usuario-by-id", id, function (res){
			$scope.user = res;
		});

		if(!find){
			$scope.error = "ERROR NO SE ENCONTRO EL USUARIO.";
		}
	}

	$scope.success = "";

	$scope.guardar = function (){
		var usr = angular.copy( $scope.user );
		model   = "?model=usuario-new&action=insert";

		// if(!usr.id){
		// 	$http.post(API + model, usr).then( function (res){
				
		// 		console.log("inser user:", res);
		// 		$scope.success = res.data.success;
				
		// 		setTimeout( function (){
		// 			$state.go('usuarios');
		// 		}, 2000);
		// 	});
		// } else {
		// 	console.log("editar usuario ::");
		// 	model = "?model=usuario-edit&action=insert"
			
		// 	// $http.post(API + model, usr).then( function (res){
				
		// 	// 	console.log("inser user:", res);
		// 	// 	$scope.success = res.data.success;
		// 	// });
		// }
		console.log("usr: ", usr);
	};
}]);